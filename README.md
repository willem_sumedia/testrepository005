project-template-cakephp
========================

Source: https://github.com/QoboLtd/project-template-cakephp

This is a template for the new CakePHP projects.

Install
-------

When starting a new CakePHP project, do the following: 

NOTE: Change the name 'YOUR_DATABASE_NAME' to 'my_app' (unless you already know your database name), you can change it later.

```bash
composer create-project qobo/project-template-cakephp testrepository005
cd testrepository005
git init
git add .
git commit -m "Initial commit"
composer exec -- phake app:install DB_NAME=YOUR_DATABASE_NAME
```

# .travis.yml

Change the DB_NAME to 'my_app' (unless you already know your database name) too in the file .travis.yml:

```
install:
    - ./bin/composer install --no-interaction --no-progress --no-suggest
    - ./vendor/bin/phake dotenv:create CHOWN_USER=$USER CHGRP_GROUP=$USER DB_NAME=my_app DB_ADMIN_USER=root DB_USER=root
    - if [ -d "config/CsvMigrations" ] || [ -d "config/Modules" ] ; then ./bin/cake validate ; fi
    - ./vendor/bin/phake app:install
```

Update
------

When you want to update your project with the latest
and greatest project-template, do the following:

```
cd testrepository005
git pull https://github.com/QoboLtd/project-template-cakephp
```


Usage
-----

### Quick

Now that you have the project template installed, check that it works
before you start working on your changes.  

NOTE: If you have not yet an environment file (.env), create one in your local clone of the repository (not in the repository itself as it contains vulnerable information, such as passwords) before moving on. A template is provided (.env.example).

Fire up the PHP web server:

```
./bin/phpserv
```

Or run it on the alternative port:

```
./bin/phpserv -H localhost -p 9000
```

In your browser navigate to [http://localhost:8000](http://localhost:8000).
You should see the standard ```phpinfo()``` page.  If you do, all parts
are in place.


Now you can develop your PHP project as per usual, but with the following
advantages:

* Support for [PHP built-in web server](http://php.net/manual/en/features.commandline.webserver.php)
* Per-environment configuration using ```.env``` file, which is ignored by git
* Powerful build system ([phake-builder](https://github.com/QoboLtd/phake-builder)) integrated
* Composer integrated with ```vendor/``` folder added to ```.gitignore``` .
* PHPUnit integrated with ```tests/``` folder and example unit tests.
* Sensible defaults for best practices - favicon.ico, robots.txt, MySQL dump, Nginx configuration, GPL, etc.

For example, you can easily automate the build process of your application
by modifying the included ```Phakefile```.  Run the following command to examine
available targets:

```
./vendor/bin/phake -T
```

As you can see, there are already some placeholders for your application's build
process.  By default, it is suggested that you have these:

* ```app:install``` - for installation process of your application,
* ```app:update``` - for the update process of the already installed application, and
* ```app:remove``` - for the application removal process and cleanup.

You can, of course, add your own, remove these, or change them any way you want.  Have a look at
[phake-builder](https://github.com/QoboLtd/phake-builder) documentation for more information on how
to use these targets and pass runtime configuration parameters.


Test
----

### PHPUnit

project-template brings quite a bit of setup for testing your projects.  The
first part of this setup is [PHPUnit](https://phpunit.de/).  To try it out,
runt the following command (don't worry if it fails, we'll get to it shortly):

```
./vendor/bin/phpunit
```

If it didn't work for you, here are some of the things to try:

* If ```phpunit``` command wasn't found, try ```composer install``` and then run the command again.  Chances are phpunit was removed during the ```app:install```, which runs composer with ```--no-dev``` parameter.
* If ```phpunit``` worked fine, but the tests failed, that's because you probably don't have a web and Selenium server running yet (more on that later).  For now, try the simplified test plan: ```phpunit --exclude-group selenium --exclude-group network```.
* If you had some other issue, please [let us know](https://github.com/QoboLtd/project-template/issues/new).

### Selenium

[Selenium](http://www.seleniumhq.org/) is a testing platform that allows one to run tests through a real browser.
Setting this up and getting an example might sound complicated, so project-template
to the rescue.  Here is what you need to do:

* Download [Selenium Server Standalone JAR file](http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar) (check ```.travis.yml``` for newer versions).
* Start the web server (in separate terminal or in background): ```php -S localhost:8000```
* Start the Selenium server (in separate terminal or in background): ```java -jar selenium-server-standalone-2.53.0.jar```

Now you can run the full test suite with:

```
./vendor/bin/phpunit
```

Or just the Selenium tests with:

```
./vendor/bin/phpunit --group selenium
```

### Travis CI

Continious Integration is a tool that helps to run your tests whenever you do any
changes on your code base (commit, merge, etc).  There are many tools that you can
use, but project-template provides an example integration with [Travis CI](https://travis-ci.org/).

Have a look at ```.travis.yml``` file, which describes the environment matrix, project installation
steps and ways to run the test suite.  For your real project, based on project-template, you'd probably
want to remove the example tests from the file.

### Examples

project-template provides a few examples of how to write and organize unit tests.  Have a look
in the ```tests/``` folder.  Now you have **NO EXCUSE** for not testing your applications!

### Configurations

#### Analytics

Load Google Analytics in your project by doing the following.

```
<?= $this->element('Snippet/google_analytics', ['ua' => 'UA goes here']); ?>
```
The parameter is optional as it defaults to App.analytics configuration.

### Bitbucket Pipelines

#### Implement your continuous integration workflow with Bitbucket Pipelines

Based on https://www.atlassian.com/continuous-delivery/continuous-integration-tutorial

It is assumed your repository is in BitBucket for this to work. Go to Pipelines by clicking on the corresponding menu item in the sidebar. 
After enabling Pipelines, pick the 'Other' template in the configuration example.

The template (bitbucket-pipelines.yml) shown looks like:

```
# This is a sample build configuration for Other.
# Check our guides at https://confluence.atlassian.com/x/5Q4SMw for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
# image: atlassian/default-latest
pipelines:
  default:
    - step:
        script:
          - echo "Everything is awesome!"
```

Click 'Next'

Bitbucket will have found the installed bitbucket-pipelines.yml that has come with the project-template-cakephp. 

SET THE DB_NAME IN bitbucket-pipelines.yml TO my_app

It looks like:

```
image: smartapps/bitbucket-pipelines-php-mysql

pipelines:
  default:
    - step:
        script:
          - service mysql start
          - if [ ! -d ~/.ssh ] ; then mkdir ~/.ssh ; fi
          - echo $MY_SSH_KEY | base64 --decode -i > ~/.ssh/id_rsa
          - echo > ~/.ssh/known_hosts
          - ssh-keyscan -t rsa bitbucket.com >> ~/.ssh/known_hosts
          - ssh-keyscan -t rsa bitbucket.org >> ~/.ssh/known_hosts
          - ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts
          - chmod -R u+rwX,go-rwX ~/.ssh
          - ./bin/composer install --no-interaction --no-progress --no-suggest
          - ./vendor/bin/phake dotenv:create CHOWN_USER=$USER CHGRP_GROUP=$USER DB_NAME=my_app DB_ADMIN_USER=root DB_ADMIN_PASS=root DB_USER=root DB_PASS=root
          - if [ -d "config/CsvMigrations" ] || [ -d "config/Modules" ] ; then ./bin/cake validate ; fi
          - ./vendor/bin/phake app:install
          - ./vendor/bin/phpunit --group example
          - ./vendor/bin/phpunit --exclude-group example
          - ./vendor/bin/phpcs

```

Exit the page by choosing 'Pipelines' from the sidebar menu in the Bitbucket repository again.

Bitbucket Pipelines will run just like you would have in your own terminal to install dependencies and run the tests, triggered on every commit.

The Pipeline will start with a status of 'In progress' and end with either a 'success' or a 'failed'. If failed, you will be sent an email notifying you about the fail.

In order for the pipeline to succeed, it needs to write to a directory 'tmp' and 'logs' seen from the root directory of the repository (so testrepository005/tmp and testrepository005/logs). Therefore create a directory 'tmp' with inside of it a file called 'empty' with no content. Also, create a directory 'logs' with inside of it a file 'empty' with no content.

In addition, for the Pipeline to succeed, it is recommended for there to be a file called '.env' in the root of the repository (so testrepository005/.env). If there is no such file (it is recommended NOT to push the .env file from the clone of the repository up to the Bitbucket), create an empty file called .env in the Bitbucket repository.

NOTE: Set the correct values for the Database default connection with the MySQL database. These should be set in testrepository005/config/app.default.php:

    /**
     * Connection information used by the ORM to connect
     * to your application's datastores.
     * Drivers include Mysql Postgres Sqlite Sqlserver
     * See vendor\cakephp\cakephp\src\Database\Driver for complete list
     */
    'Datasources' => [
        'default' => [
        
            'username' => 'root', // was: my_app
            'password' => 'root', // was: secret
            'database' => 'my_app',
            
        ],
        
So, above replace 'my_app' with for example 'root' and the password 'secret' with the corresponding password (e.g. 'root').

Make the same changes for the 'username' and 'password' in the Test connection section, as well as the database name set to 'my_app' AS THIS IS THE DATABASE NAME SET IN bitbucket-pipelines.yml as shown below (which is also to be found in testrepository005/config/app.default.php):

        /**
         * The test connection is used during the test suite.
         */
        'test' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'nonstandard_port_number',
            'username' => 'root', // was: my_app
            'password' => 'root', // was: secret
            'database' => 'my_app', // was: test_myapp
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ]

##### DatabaseLog plugin

Next, add the following entry to the Datasources section, it is used by the DatabaseLog plugin:

        /**
         * The database_log connection stores DatabaseLog data.
         */
        'database_log' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'nonstandard_port_number',
            'username' => 'root', // was: my_app
            'password' => 'root', // was: secret
            'database' => 'my_app', // recommended to NOT use the application database (e.g. my_app) here, but a separate database (e.g. database_log)
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ],        
        
In addition, add this to testrepository005/config/app.default.php, as it is used by the DatabaseLog plugin:

    /**
     * Configures DatabaseLog options
     */
    'DatabaseLog' => [
        'datasource' => 'database_log', // DataSource to use for DatabaseLog (e.g. database_log), recommended NOT to set it to the application database (e.g. my_app)
    ],    

##### DebugKit plugin

Next, add the following entry to the Datasources section, it is used by the DebugKit plugin:
        
        /**
         * The debug_kit connection stores DebugKit meta-data.
         */
        'debug_kit' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'nonstandard_port_number',
            'username' => 'root', // was: my_app
            'password' => 'root', // was: secret
            'database' => 'my_app', // recommended to NOT use the application database (e.g. my_app) here, but a separate database (e.g. debug_kit)
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ],
        
In addition, add this to testrepository005/config/app.default.php, as it is used by the DebugKit plugin:

    /**
     * Configures DebugKit options
     */
    'DebugKit' => [
        'datasource' => 'debug_kit', // DataSource to use for DebugKit (e.g. debug_kit), recommended NOT to set it to the application database (e.g. my_app)
    ],


### ShipIT

You can automate anything with Shipit (https://github.com/shipitjs/shipit) but most of the time you will want to deploy your project.

See a tutorial at http://evilmousestudios.com/casting-off-with-shipit/

Install
-------

Locally (make sure there is a package.json in the root directory, or create one with ```npm init```)

```javascript
npm install --save-dev shipit-cli
```

Install any required additional modules:

```javascript
npm install --save-dev shipit-deploy
```

Getting Started
---------------

Once shipit is installed, you must create a ***shipitfile.js***.

NOTE: 

- replace 'myserver.com' with the domain of your staging server.
- replace 'user' with the user with whom to log onto the server.

```javascript
module.exports = function (shipit) {
  require('shipit-deploy')(shipit);   

  shipit.initConfig({
    default: {
      workspace: '/tmp/github-monitor', // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
      dirToCopy: '', // Define directory within the workspace, which should be deployed.
      deployTo: '/tmp/deploy_to', // Define the remote path where the project will be deployed. A directory releases is automatically created. A symlink current is linked to the current release.
      repositoryUrl: 'https://github.com/user/repo.git', // Git URL of the project repository.
      ignores: ['.git', 'node_modules'], // An array of paths that match ignored files. These paths are used in the rsync command.
      rsync: ['--del'],
      keepReleases: 2, // Number of releases to keep on the remote server.
      deleteOnRollback: false, // Whether or not to delete the old release when rolling back to a previous release.
      key: '/path/to/key', // Path to SSH key
      shallowClone: true, // Perform a shallow clone. Default: false.
      updateSubmodules: false, // Update submodules. Default: false.
      // gitConfig: Object, // Custom git configuration settings for the cloned repo.
      // gitLogFormat: %h: %s - %an // Log format to pass to git log. Used to display revision diffs in pending task. Default: %h: %s - %an.
      // rsyncFrom: , // Optional, When deploying from Windows, prepend the workspace path with the drive letter. For example /d/tmp/workspace if your workspace is located in d:\tmp\workspace. By default, it will run rsync from the workspace folder.
      // copy: , // Parameter to pass to cp to copy the previous release. Non NTFS filesystems support -r. Default: -a
    },
    staging: {
      servers: 'user@myserver.com', // Servers on which the project will be deployed. Pattern must be user@myserver.com if user is not specified (myserver.com) the default user will be "deploy".
      workspace: '/home/vagrant/website', // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
      branch: 'dev' // Tag, branch or commit to deploy.
    },
    production: {
      servers: [{
        host: 'app1.myproject.com',
        user: 'john',
      }, {
        host: 'app2.myproject.com',
        user: 'rob',
      }],
      branch: 'production', // Tag, branch or commit to deploy.
      workspace: '/var/www/website' // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
    }
  });

  shipit.task('build', function () {
    console.log("task 'build' called");
    shipit.emit('built');
  });

  shipit.on('built', function () {
    console.log("event 'built' caught");   
    // e.g shipit.start('start-server');
  });

  shipit.task('pwd', function () {
    return shipit.remote('pwd');
  });
  
};
```

Deploy using ShipIt
-------------------

To deploy on staging, you must use the following command :

NOTE: 

- the environment is: staging
- the task is: deploy

```javascript
shipit staging deploy
```

You can rollback to the previous releases with the command :

```javascript
shipit staging rollback
```
