<?php
use Cake\Core\Configure;
use CsvMigrations\Swagger\Analyser;

return [
    'Swagger' => [
        'ui' => [
            'title' => (getenv('PROJECT_NAME') ?: basename(ROOT)) . ': API Documentation',
            'validator' => true,
            'api_selector' => true,
            'route' => '/swagger/',
            'schemes' => ['http', 'https']
        ],
        'docs' => [
            'crawl' => Configure::read('debug'),
            'route' => '/swagger/docs/',
            'cors' => [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST',
                'Access-Control-Allow-Headers' => 'X-Requested-With'
            ]
        ],
        'library' => [
            'api' => [
                'include' => [
                    ROOT . DS . 'src' . DS . 'Controller' . DS . 'Api'
                ]
            ]
        ],
        'analyser' => new Analyser()
    ]
];
