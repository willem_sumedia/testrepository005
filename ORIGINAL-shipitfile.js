module.exports = function (shipit) {
  require('shipit-deploy')(shipit);   

  shipit.initConfig({
    default: {
      workspace: '/tmp/github-monitor', // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
      dirToCopy: '', // Define directory within the workspace, which should be deployed.
      deployTo: '/tmp/deploy_to', // Define the remote path where the project will be deployed. A directory releases is automatically created. A symlink current is linked to the current release.
      repositoryUrl: 'https://github.com/user/repo.git', // Git URL of the project repository.
      ignores: ['.git', 'node_modules'], // An array of paths that match ignored files. These paths are used in the rsync command.
      rsync: ['--del'],
      keepReleases: 2, // Number of releases to keep on the remote server.
      deleteOnRollback: false, // Whether or not to delete the old release when rolling back to a previous release.
      key: '/path/to/key', // Path to SSH key
      shallowClone: true, // Perform a shallow clone. Default: false.
      updateSubmodules: false, // Update submodules. Default: false.
      // gitConfig: Object, // Custom git configuration settings for the cloned repo.
      // gitLogFormat: %h: %s - %an // Log format to pass to git log. Used to display revision diffs in pending task. Default: %h: %s - %an.
      // rsyncFrom: , // Optional, When deploying from Windows, prepend the workspace path with the drive letter. For example /d/tmp/workspace if your workspace is located in d:\tmp\workspace. By default, it will run rsync from the workspace folder.
      // copy: , // Parameter to pass to cp to copy the previous release. Non NTFS filesystems support -r. Default: -a
    },
    staging: {
      servers: 'user@myserver.com', // Servers on which the project will be deployed. Pattern must be user@myserver.com if user is not specified (myserver.com) the default user will be "deploy".
      workspace: '/home/vagrant/website', // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
      branch: 'dev' // Tag, branch or commit to deploy.
    },
    production: {
      servers: [{
        host: 'app1.myproject.com',
        user: 'john',
      }, {
        host: 'app2.myproject.com',
        user: 'rob',
      }],
      branch: 'production', // Tag, branch or commit to deploy.
      workspace: '/var/www/website' // Define a path to an empty directory where Shipit builds it's syncing source. Beware to not set this path to the root of your repository as shipit-deploy cleans the directory at the given path as a first step.
    }
  });

  shipit.task('build', function () {
    console.log("task 'build' called");
    shipit.emit('built');
  });

  shipit.on('built', function () {
    console.log("event 'built' caught");   
    // e.g shipit.start('start-server');
  });

  shipit.task('pwd', function () {
    return shipit.remote('pwd');
  });
  
};